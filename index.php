<?php
	session_start();
	
	define('ROOT', dirname(__FILE__));
	
	include('php/Database/Connection.php');
	
	$DB = new spojenie();
	
	$podstranka = 'klavesnica';
	if (isset($_GET['podstranka'])){
		$podstranka = $_GET['podstranka'];
	}
	
?>
<html lang="sk">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<base href="http://<?= $_SERVER['HTTP_HOST'] . preg_replace("/(\/\w+\.\w+)$/", "/", $_SERVER['PHP_SELF']); ?>">
		
		<link rel="stylesheet" href="css/style.css"/>
		<link rel="stylesheet" href="css/keyboard.css"/>
		
		<link rel="stylesheet" href="lib/css/bootstrap.min.css">
		<link rel="stylesheet" href="lib/css/bootstrap-theme.min.css">

		
	    <script src="lib/js/jquery-1.11.2.min.js"></script>
	    <script src="lib/js/jquery.textareaAutoResize.js"></script>
		<script src="lib/js/bootstrap.min.js"></script>
		<script src="lib/js/jquery.animate-colors.js"></script>
		<script src="lib/js/jquery.animate-colors-min.js"></script>
		
		<script src="js/sova.js"></script>
		<script src="js/generator_uloh.js"></script>
		<script src="js/keyboard.js"></script>
		
		<title>Softvér pre vzdelávanie</title>
	</head>

	<body>
		<header class="col-xs-12 col-sm-12 col-md-12 col-lg-12">		
			<nav class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 nopadding">
				<ul>
					<li><a href="klavesnica/" 	class="<?=($podstranka == 'klavesnica' 	? 'header_visited' : '' );?>">Vyskúšaj si ma</a></li>
					<li><a href="pismena/"		class="<?=($podstranka == 'pismena' 	? 'header_visited' : '' );?>">Písmená</a></li>
					<li><a href="slova/"  		class="<?=($podstranka == 'slova'  		? 'header_visited' : '' );?>">Slová</a></li>
					<li><a href="vety/" 		class="<?=($podstranka == 'vety' 		? 'header_visited' : '' );?>">Vety</a></li>			
					<li><a href="navod/" 		class="<?=($podstranka == 'navod' 		? 'header_visited' : '' );?>">Ako na to</a></li>			
					<li><a href="edit/" 		class="<?=($podstranka == 'edit' 		? 'header_visited' : '' );?>">Upraviť</a></li>			
				</ul>
			</nav>
		</header>
		
		<div class="main col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding">
			<?php
				switch ($podstranka) {
					case 'klavesnica':
						include('php/View/klavesnica.php');
						break;
					case 'pismena':
						include('php/View/pismena.php');
						break;
					case 'slova':
						include('php/View/slova.php');
						break;
					case 'vety':
						include('php/View/vety.php');
						break;
					case 'navod':
						include('php/View/navod.php');
						break;
					case 'edit':
						include('php/View/edit.php');
						break;
					
					default:
						include('php/View/klavesnica.php');
				}
			?>
		</div>
			
		<footer>
			<p>Michal Sejč, Tomáš Kopernický, Iveta Csicsolová - Softvér pre vzdelávanie</p>
			<p class="footer_score" id="skore">0</p>
		</footer>
	</body>
</html>