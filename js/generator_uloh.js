var diakritika_kriterum = false;
var spec_znaky_kriterum = false;
var velke_pism_kriterum = false;
var pole_pochval 		= ['Výborne!', 'Super!', 'Správne!', 'Vynikajúco!', 'Si hviezda :)', 'Exceletne!', 'Ide ti to :)'];
var pole_nespravnosti	= ['Skontroluj si to.', 'Ešte si to skontroluj.'];
var skore				= 0;
var timer_spravnosti;
var timer_nespravnosti;
var predosle = '';

function vyber_slovo() {
	/*
		input  => pole objektov slov
		output => jedno nahodne slovo podla kriterii
	*/
	
	let slovo = slova[Math.floor(Math.random() * slova.length)];
	
	if (diakritika_kriterum) {
		if (velke_pism_kriterum) {
			return slovo['slovo'];	
		} else {
			if (slovo['slovo'][0] === slovo['slovo'][0].toUpperCase()) {
				return vyber_slovo();
			} else {
				return slovo['slovo'];
			}
		}
	} else {
		if (je_diakritika(slovo['slovo'])) {
			return vyber_slovo();		
		} else {		
			if (velke_pism_kriterum) {
				return slovo['slovo'];	
			} else {
				if (slovo['slovo'][0] === slovo['slovo'][0].toUpperCase()) {
					return vyber_slovo();
				} else {
					return slovo['slovo'];
				}
			}
		}
	}
}

function vyber_vetu() {
	let veta = vety[Math.floor(Math.random() * vety.length)];
	
	if ((spec_znaky_kriterum) && (diakritika_kriterum)) {
		if ((je_spec_znak(veta['veta'])) && (je_diakritika(veta['veta'].slice(0, -1)))) {
			return veta['veta'];
		} else {
			return vyber_vetu();
		}
	}
	
	if ((!spec_znaky_kriterum) && (diakritika_kriterum)) {
		if ((!je_spec_znak(veta['veta'])) && (je_diakritika(veta['veta'].slice(0, -1)))) {
			return veta['veta'];
		} else {
			return vyber_vetu();
		}
	}
	
	if ((spec_znaky_kriterum) && (!diakritika_kriterum)) {
		if ((je_spec_znak(veta['veta'])) && (!je_diakritika(veta['veta'].slice(0, -1)))) {
			return veta['veta'];
		} else {
			return vyber_vetu();
		}
	}
	
	if ((!spec_znaky_kriterum) && (!diakritika_kriterum)) {
		if ((!je_spec_znak(veta['veta'])) && (!je_diakritika(veta['veta'].slice(0, -1)))) {
			return veta['veta'];
		} else {
			return vyber_vetu();
		}
	}
	
	return veta['veta'];
}

function vyber_pismeno() {
	let vyberovePole = [];
	
	if (velke_pism_kriterum) {
		vyberovePole = vyberovePole.concat(v_pismena);
	}
	
	if (diakritika_kriterum) {	
		if (velke_pism_kriterum) {
			vyberovePole = vyberovePole.concat(diak_v_pismena);	
		} else {
			vyberovePole = vyberovePole.concat(diak_m_pismena);
		}
	}

	if (spec_znaky_kriterum) {
		vyberovePole = vyberovePole.concat(spec_znaky);
	}
	
	if (vyberovePole.length < 1) {
		vyberovePole = m_pismena;
	}

	return vyberovePole[Math.floor(Math.random() * vyberovePole.length)];
}

function timer_skontroluj_riesenie(typ) {
	clearTimeout(timer_spravnosti);
	
	timer_spravnosti = setTimeout(function(){ skontroluj_riesenie(typ); }, 700);
}

function skontroluj_riesenie(typ) {
	let w = document.getElementById('write').value;
	
	w = w.replace(/^\s+|\s+$/g, '');
	w.trim();
	
	if (document.getElementById('text_predlohy_textarea').value == w) {
		clearTimeout(timer_nespravnosti);
		zobraz_bublinu();
		zmen_text_sovy(pole_pochval[Math.floor(Math.random() * pole_pochval.length)]);
		
		setTimeout(function(){ dalsia_uloha(typ); }, 1500);
		
		skore++;
		zmen_skore(skore);
	}
}

function zmen_skore(s) {
	document.getElementById("skore").innerHTML = s;
}

function dalsia_uloha(typ) {
	var aktualne;
	zmen_text_sovy('Pokračujeme ďalšou úlohou :)');
	vynuluj_text_zositu();
	switch (typ) {
		case 'slova':
			aktualne = vyber_slovo();
			if (aktualne == predosle) {
				dalsia_uloha(typ);
				break;
			} else {
				zmen_text_predlohy(aktualne);
				predosle = aktualne;
			}
			
			break;
		case 'vety':
			aktualne = vyber_vetu();
			if (aktualne == predosle) {
				dalsia_uloha(typ);
				break;
			} else {
				zmen_text_predlohy(aktualne);
				predosle = aktualne;
			}
			break;
		case 'pismena':
			aktualne = vyber_pismeno();
			if (aktualne == predosle) {
				dalsia_uloha(typ);
				break;
			} else {
				zmen_text_predlohy(aktualne);
				predosle = aktualne;
			}
			break;
	}
	
	setTimeout(function(){ schovaj_bublinu(); }, 2000);
}

function je_spec_znak(vstup) {
	var regex = new RegExp("[\$\+\*\?\#\"\,]");
	var str   = vstup.replace(/\s/g, '');

	if (regex.test(str)) {
		return true;
	}
	
	return false;
}

function je_diakritika(vstup) {
	var regex = new RegExp('[ÁČĎÉÍĹĽŇÓŔŠŤÚÝŽáäčďéíĺľňóôŕšťúýž]');
	var str   = vstup.replace(/\s/g, '');
	
	if (regex.test(str)) {
		return true;
	}
	
	return false;
}

function zmen_kriterium_diakritika() {
	diakritika_kriterum = !diakritika_kriterum;
}

function zmen_kriterium_specialne_znaky() {
	spec_znaky_kriterum = !spec_znaky_kriterum;
}

function zmen_kriterium_velkost_pismen() {
	velke_pism_kriterum = !velke_pism_kriterum;
}

function zmen_text_predlohy(text) {
	document.getElementById('text_predlohy_textarea').value = text;
}

function vynuluj_text_zositu() {
	document.getElementById('write').value = '';
}

function zapni_timer_nespravnosti() {
	clearTimeout(timer_nespravnosti);
	
	timer_nespravnosti = setTimeout(function(){ ukaz_hlasku_nespravnosti(); }, 3000);
}

function ukaz_hlasku_nespravnosti () {
	zobraz_bublinu();
	zmen_text_sovy(pole_nespravnosti[Math.floor(Math.random() * pole_nespravnosti.length)]);
}

function zmen_zobrazenie_moznosti() {
	if ((document.getElementById("moznosti_knihy").style.display == 'block') || (document.getElementById("moznosti_knihy").style.display == '')) {
		document.getElementById("moznosti_knihy").style.display = 'none';
	} else {
		document.getElementById("moznosti_knihy").style.display = 'block';
	}
}

function zmen_zobrazenie_klavesnice() {
	if ((document.getElementById("klavesnica").style.display == 'block') || (document.getElementById("klavesnica").style.display == '')) {
		document.getElementById("klavesnica").style.display = 'none';
	} else {
		document.getElementById("klavesnica").style.display = 'block';
	}
}
