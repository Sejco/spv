<?php
class spojenie{
	protected $spoj;
	protected $result;
	
	public function __construct(){
		if ((!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/' == 'http://localhost/'){
			$this->spoj = mysqli_connect('localhost', 'root', '', 'spv');
		} else {
			// udaje k ostrej verzii
			$this->spoj = mysqli_connect('localhost', 'sejco', '34455765473', 'spv');
		}
		if(!$this->spoj){
			die(mysqli_connect_error());
		}
		mysqli_query($this->spoj, "SET CHARACTER SET 'utf8'");
	}
	
	public function odpoj(){
		mysqli_close($this->spoj);
	}
	
	public function __destruct(){
		$this->odpoj();
	}
	
	public function vratSpojenie(){
		return $this->spoj;
	}
	
	public function id(){
		return mysqli_insert_id($this->spoj);
	}
	
	public function query($sql){
		$this->result = mysqli_query($this->spoj, $sql);
		if($this->result === false){
			die(mysqli_error($this->spoj)." SQL: $sql");
		}
		return $this->result;
	}
	
	public function fetch_single($sql){
		$this->query($sql);
		return mysqli_fetch_assoc($this->result);
	}
	
	public function fetch_multi($sql){
		$this->query($sql);
		$rows = array();
		while($row = mysqli_fetch_assoc($this->result)){
			$rows[] = $row;
		}
		return $rows;
	}
	
}

class DEBUGspojenie extends spojenie{
	private $dopyt = array();
	
	public function __construct(){
		parent::__construct();
	}
	
	public function __destruct(){
		parent::__destruct();
	}
	
	public function vratDopyt(){
		return $this->dopyt;
	}
	
	public function vratPocetDopytov(){
		return count($this->dopyt);
	}
	
	public function query($sql){
		$this->result = mysqli_query($this->spoj, $sql);
		if($this->result === false){
			die(mysqli_error($this->spoj)." SQL: $sql");
		}
		$this->dopyt[] = $sql;
		return $this->result;
	}
}
?>