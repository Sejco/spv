<?php
	class Cotroller_edit { 
		public $slova;
		public $vety;
	
		function __construct() {
			if (isset($_GET['delete'])){	
				if ($_GET['delete'] == 'delslovo') {
					$this->vymaz_slovo($_GET['id']);
				} elseif ($_GET['delete'] == 'delveta') {
					$this->vymaz_vetu($_GET['id']);
				}
				
				header("Refresh:0; url=/edit/");
			}	
			
			if (isset($_POST['pridaj_slovo'])){	
				$slova_pole = explode(",", $_POST['slova']);
				
				for ($i = 0; $i < count($slova_pole); $i++) {
					if (!empty($slova_pole[$i])) {
						$this->pridaj_slovo(trim($slova_pole[$i]));
					}
				}
				
				header("Refresh:0; url=/edit/");
			}	
			
			if (isset($_POST['pridaj_vetu'])){	
				if (!empty($_POST['veta'])) {
					$this->pridaj_vetu($_POST['veta']);
				}
	
				header("Refresh:0; url=/edit/");
			}	
			
			$this->slova = $this->nacitaj_vsetky_slova();
			$this->vety	 = $this->nacitaj_vsetky_vety();
		}
		
		function pridaj_slovo($slovo) {
			$sql = "INSERT INTO slova (slovo) VALUES ('$slovo')";
	
			$GLOBALS['DB']->query($sql);
		}
		
		function pridaj_vetu($veta) {
			$sql = "INSERT INTO vety (veta) VALUES ('$veta')";
	
			$GLOBALS['DB']->query($sql);
		}
		
		function vymaz_vetu($id) {
			$sql = "DELETE FROM vety WHERE id = '$id'";

			$GLOBALS['DB']->query($sql);
		}
		
		function vymaz_slovo($id) {
			$sql = "DELETE FROM slova WHERE id = '$id'";

			$GLOBALS['DB']->query($sql);
		}	

		public static function nacitaj_vsetky_slova() {
			$sql = "SELECT * FROM slova ORDER BY slovo ASC";
		
			return $GLOBALS['DB']->fetch_multi($sql);
		}
		
		public static function nacitaj_vsetky_vety() {
			$sql = "SELECT * FROM vety";
		
			return $GLOBALS['DB']->fetch_multi($sql);
		}		
		
	}
?>	


