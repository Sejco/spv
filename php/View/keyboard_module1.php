<div id="keyBoard1">
	  <ul id="keyboard">
        <li id="Backquote" class="symbol"><span id="apostrof" hidden>´</span><span id="quote" hidden>ˇ</span><span class="off">;</span><span class="on">°</span></li>
        <li id="Digit1" class="symbol"><span id="apostrof" hidden>´</span><span id="quote" hidden>ˇ</span><span class="off">+</span><span class="on">1</span></li>
        <li id="Digit2" class="symbol"><span id="apostrof" hidden>´</span><span id="quote" hidden>ˇ</span><span class="off">ľ</span><span class="on">2</span></li>
        <li id="Digit3" class="symbol"><span id="apostrof" hidden>´</span><span id="quote" hidden>ˇ</span><span class="off">š</span><span class="on">3</span></li>
        <li id="Digit4" class="symbol"><span id="apostrof" hidden>´</span><span id="quote" hidden>ˇ</span><span class="off">č</span><span class="on">4</span></li>
        <li id="Digit5" class="symbol"><span id="apostrof" hidden>´</span><span id="quote" hidden>ˇ</span><span class="off">ť</span><span class="on">5</span></li>
        <li id="Digit6" class="symbol"><span id="apostrof" hidden>´</span><span id="quote" hidden>ˇ</span><span class="off">ž</span><span class="on">6</span></li>
        <li id="Digit7" class="symbol"><span id="apostrof" hidden>´</span><span id="quote" hidden>ˇ</span><span class="off">ý</span><span class="on">7</span></li>
        <li id="Digit8" class="symbol"><span id="apostrof" hidden>´</span><span id="quote" hidden>ˇ</span><span class="off">á</span><span class="on">8</span></li>
        <li id="Digit9" class="symbol"><span id="apostrof" hidden>´</span><span id="quote" hidden>ˇ</span><span class="off">í</span><span class="on">9</span></li>
        <li id="Digit0" class="symbol"><span id="apostrof" hidden>´</span><span id="quote" hidden>ˇ</span><span class="off">é</span><span class="on">0</span></li>
        <li id="Minus" class="symbol"><span id="apostrof" hidden>´</span><span id="quote" hidden>ˇ</span><span class="off">=</span><span class="on">%</span></li>
        <li id="Equal" class="symbol"><span id="apostrof" hidden>´</span><span id="quote" hidden>ˇ</span><span class="off">´</span><span class="on">ˇ</span></li>
		<li id="Backslash" class="symbol"><span id="apostrof" hidden>´</span><span id="quote" hidden>ˇ</span><span class="off">ň</span><span class="on">)</span></li>
        <li id="Backspace" class="delete lastitem">delete</li>
        <li id="TTab" class="tab">tab</li>
        <li id="KeyQ" class="letter"><span id="quote" hidden>ˇq</span><span class="off">q</span><span id="apostrof" hidden>´q</span></li>
        <li id="KeyW" class="letter"><span id="quote" hidden>ˇw</span><span class="off">w</span><span id="apostrof" hidden>´w</span></li>
        <li id="KeyE" class="letter"><span id="quote" hidden>ě</span><span class="off">e</span><span id="apostrof" hidden>é</span></li>
        <li id="KeyR" class="letter"><span id="quote" hidden>ř</span><span class="off">r</span><span id="apostrof" hidden>ŕ</span></li>
        <li id="KeyT" class="letter"><span id="quote" hidden>ť</span><span class="off">t</span><span id="apostrof" hidden>´t</span></li>
        <li id="KeyY" class="letter"><span id="quote" hidden>ˇy</span><span class="off">y</span><span id="apostrof" hidden>ý</span></li>
        <li id="KeyU" class="letter"><span id="quote" hidden>ˇu</span><span class="off">u</span><span id="apostrof" hidden>ú</span></li>
        <li id="KeyI" class="letter"><span id="quote" hidden>ˇi</span><span class="off">i</span><span id="apostrof" hidden>í</span></li>
        <li id="KeyO" class="letter"><span id="quote" hidden>ˇo</span><span class="off">o</span><span id="apostrof" hidden>ó</span></li>
        <li id="KeyP" class="letter"><span id="quote" hidden>ˇp</span><span class="off">p</span><span id="apostrof" hidden>´p</span></li>
        <li id="BracketLeft" class="symbol"><span id="quote" hidden>ˇ</span><span id="apostrof" hidden>´</span><span class="off">ú</span><span class="on">/</span></li>
        <li id="BracketRight" class="symbol"><span id="quote" hidden>ˇ</span><span id="apostrof" hidden>´</span><span class="off">ä</span><span class="on">(</span></li>
        <li class="enter-top" ></li>
        <li id="CapsLock" class="capslock">caps lock</li>
        <li id="KeyA" class="letter"><span id="quote" hidden>ˇa</span><span class="off">a</span><span id="apostrof" hidden>á</span></li>
        <li id="KeyS" class="letter"><span id="quote" hidden>š</span><span class="off">s</span><span id="apostrof" hidden>ś</span></li>
        <li id="KeyD" class="letter"><span id="quote" hidden>ď</span><span class="off">d</span><span id="apostrof" hidden>´d</span></li>
        <li id="KeyF" class="letter"><span id="quote" hidden>ˇf</span><span class="off">f</span><span id="apostrof" hidden>´f</span></li>
        <li id="KeyG" class="letter"><span id="quote" hidden>ˇg</span><span class="off">g</span><span id="apostrof" hidden>´g</span></li>
        <li id="KeyH" class="letter"><span id="quote" hidden>ˇh</span><span class="off">h</span><span id="apostrof" hidden>´h</span></li>
        <li id="KeyJ" class="letter"><span id="quote" hidden>ˇj</span><span class="off">j</span><span id="apostrof" hidden>´j</span></li>
        <li id="KeyK" class="letter"><span id="quote" hidden>ˇk</span><span class="off">k</span><span id="apostrof" hidden>´k</span></li>
        <li id="KeyL" class="letter"><span id="quote" hidden>ľ</span><span class="off">l</span><span id="apostrof" hidden>ĺ</span></li>
        <li id="Semicolon" class="symbol specialLetter"><span id="quote" hidden>ˇ</span><span id="apostrof" hidden>´</span><span class="off">ô</span><span class="on">&quot</span><span id="special" hidden>$</span></li>
        <li id="Quote" class="symbol"><span id="quote" hidden>ˇ</span><span id="apostrof" hidden>´</span><span class="off">§</span><span class="on">!</span></li>
        <li id="Enter" class="enter lastitem">Enter</li>
        <li id="ShiftLeft" class="left-shift">shift</li>
        <li id="IntlBackslash" class="symbol specialLetter"><span id="quote" hidden>ˇ</span><span id="apostrof" hidden>´</span><span class="off">&amp</span><span class="on">*</span><span id="special" hidden>&lt</span></li>
        <li id="KeyZ" class="letter specialLetter"><span id="quote" hidden>ž</span><span class="off">z</span><span id="special" hidden>&gt</span><span id="apostrof" hidden>ź</span></li>
        <li id="KeyX" class="letter specialLetter"><span id="quote" hidden>ˇx</span><span class="off">x</span><span id="special" hidden>#</span><span id="apostrof" hidden>´x</span></li>
        <li id="KeyC" class="letter specialLetter"><span id="quote" hidden>č</span><span class="off">c</span><span id="special" hidden>&</span><span id="apostrof" hidden>ć</span></li>
        <li id="KeyV" class="letter specialLetter"><span id="quote" hidden>ˇv</span><span class="off">v</span><span id="special" hidden>@</span><span id="apostrof" hidden>´v</span></li>
        <li id="KeyB" class="letter specialLetter"><span id="quote" hidden>ˇb</span><span class="off">b</span><span id="special" hidden>{</span><span id="apostrof" hidden>´b</span></li>
        <li id="KeyN" class="letter specialLetter"><span id="quote" hidden>ň</span><span class="off">n</span><span id="special" hidden>}</span><span id="apostrof" hidden>ń</span></li>
        <li id="KeyM" class="letter"><span id="quote" hidden>ˇ</span><span id="apostrof" hidden>´</span>m</li>
        <li id="Comma" class="symbol specialLetter"><span id="quote" hidden>ˇ</span><span id="apostrof" hidden>´</span><span class="off">,</span><span class="on">?</span><span id="special" hidden>&lt</span></li>
        <li id="Period" class="symbol specialLetter"><span id="quote" hidden>ˇ</span><span id="apostrof" hidden>´</span><span class="off">.</span><span class="on">:</span><span id="special" hidden>&gt</span></li>
        <li id="Slash" class="symbol specialLetter" ><span id="quote" hidden>ˇ</span><span id="apostrof" hidden>´</span><span class="off">-</span><span class="on">_</span><span id="special" hidden>*</span></li>
        <li id="ShiftRight" class="right-shift lastitem">shift</li>
		<li id="ControlLeft" class="ctrl-left">Ctrl</li>
        <li id="MetaLeft" class="windows">win</li>
        <li id="AltLeft" class="alt">Alt</li>
		<li id="Space" class="space">&nbsp;(Space)</li>
        <li id="AltRight" class="alt">Alt</li>
        <li id="MetaRight" class="windows">win</li>
        <li id="ControlRight" class="ctrl lastitem">Ctrl</li>    
    </ul>
	<ul id="arrow">
		<li id="ArrowUp" class="up">Up</li>
        <li id="ArrowLeft" class="left">L</li>
        <li id="ArrowDown" class="down">D</li>
        <li id="ArrowRight" class="right">R</li>        
	</ul>
	<ul id="numlock">
		<li id="NumLock" class="num">NumL</li>
		<li id="NumpadDivide" class="num">/</li>
		<li id="NumpadMultiply" class="num">*</li>
		<li id="NumpadSubtract" class="num">-</li>
		<li id="Numpad7" class="num">7</li>
        <li id="Numpad8" class="num">8</li>
        <li id="Numpad9" class="num">9</li>
		<li id="NumpadAdd" class="" style="height:85px;">+</li>		
		 <li id="Numpad4" class="num">4</li>
        <li id="Numpad5" class="num">5</li>
		<li id="Numpad6" class="num">6</li>
		<li id="Numpad1" class="num">1</li>
        <li id="Numpad2" class="num">2</li>
        <li id="Numpad3" class="num">3</li>
		<li id="NumpadEnter" class="num"style="height:85px;">Enter</li>		
       <li id="Numpad0" class="num" style="width:85px;">0</li>
	   <li id="NumpadDecimal" class="num" >.</li>		
		
       <div id="ActionBox" hidden>
		Skontroluj Num Lock
		</div>
	</ul>
</div>
<textarea maxlength="60" id="textarea1" hidden></textarea>
<script>
	var Keyboard = new KeyBoard("textarea1","#keyBoard1"); 
	var first=1;
	function animate(){
		Keyboard.animateCharacter("Ť");
		first=24000;
		start();
	}
	function start(){
		setTimeout(function(){
			
			animate();
   
		}, first)
	}
	start();
	
</script>
