<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 margin-top-30">
	<div class="row">
		<div class="col-md-12 bottom-10">
			<div class="col-md-6 col-sm-6 col-xs-6" style="padding-left:180px; height:65px;">
				<img src="../images/kniha.png" alt="Kniha" id="kniha" width="60" height="60" onclick="zmen_zobrazenie_moznosti();">
				<div id="moznosti_knihy" style="display:none;">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding-left">
					<?php
						if ($podstranka == 'slova') {
							?>
								<div class="checkbox">
									<label><input type="checkbox" value="" onchange="zmen_kriterium_diakritika()">Slová s diakritikou</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" value="" onchange="zmen_kriterium_velkost_pismen()">Slová s veľkým písmenom</label>
								</div>
							<?php
						} elseif ($podstranka == 'vety') {
							?>
								<div class="checkbox">
									<label><input type="checkbox" value="" onchange="zmen_kriterium_diakritika()">Vety obsahujúce diakritiku</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" value="" onchange="zmen_kriterium_specialne_znaky()">Vety obsahujúce špeciálne znaky</label>
								</div>
							<?php
						} elseif ($podstranka == 'pismena') {
							?>
								<div class="checkbox">
									<label><input type="checkbox" value="" onchange="zmen_kriterium_velkost_pismen()">Veké písmená</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" value="" onchange="zmen_kriterium_diakritika()">Písmená s diakritikou</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" value="" onchange="zmen_kriterium_specialne_znaky()">Špeciálne znaky</label>
								</div>
							<?php
						}
					?>		
						<div class="checkbox">
							<label><input type="checkbox" value="" onchange="zmen_zobrazenie_klavesnice()" checked>Klávesnica</label>
						</div>
					</div>
				</div>
				
			</div>
			<div class="col-md-6 col-sm-6 col-xs-6" style="height:65px;">
				<img src="../images/sova.png" alt="Sova" id="sova" width="60" height="60">
				<p class="bubble nomargin-bottom" id="rady_sovy" style="display:none;"></p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<center>
				<div id="text_predlohy" style="display: inline-block;">
					<textarea maxlength="100" id="text_predlohy_textarea" class="<?=(($podstranka == 'pismena') ? 'text_predlohy_pismena' : '');?>" readonly></textarea>
					<?php
						if ($podstranka == 'pismena') {
							?>
								<div id="napoveda">
									<button type="button" class="btn btn-primary" onclick="Keyboard.animateCharacter(text_predlohy_textarea.value);">Nápoveda</button>
								</div>
							<?php
						}					
					?>
				</div>
				
				<div id="text_zositu" style="display: inline-block;">
					<textarea maxlength="60" id="write" class="<?=(($podstranka == 'pismena') ? 'text_predlohy_pismena' : '');?>" oninput="Keyboard.stopAnimate(); timer_skontroluj_riesenie('<?=$podstranka;?>'); zapni_timer_nespravnosti();"></textarea>
				</div>
			</center>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="klavesnica">
				<?php
					include('php/View/keyboard_module.php');
				?>
			</div>
		</div>
	</div>
</div>

<script>
	function nastav_focus_text_input() {
		document.getElementById("write").focus();
		setTimeout(function(){ nastav_focus_text_input(); }, 300);
	}
	nastav_focus_text_input();
</script>