<?php	
	$default_heslo = '12345';
	
	include('php/Controller/cotroller_edit.php');
	
	if (isset($_POST['heslo'])) {
		if ($_POST['heslo'] == $default_heslo) {
			$_SESSION['login'] = true;
		}
	}
	
	
	if (!isset($_SESSION['login'])) {
		?>
			<div class="login_form_div">
				<form class="center" method="post">
					<div class="form-group">
						<label class="control-label padding-10" for="heslo"><strong>Zadajte heslo</strong></label>	
						<input type="password" class="form-control" id="heslo" name="heslo" style="width:200px;">
					</div>
					<div class="form-group"> 
						<button type="submit" class="btn btn-primary" name="prihlasit" style="width:200px;">Potvrdiť</button>
					</div>
				</form>
			</div>
		<?php
	} else {
		$edit = new Cotroller_edit();	
		?>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 margin-top-30">
				<h1>Slová</h1>
				<div class="padding-side-15">
					<table class="table table-striped table-style-1 table-hover table-bordered table-condensed">
						<thead>
							<tr>
								<th class="col-md-8">Slovo</th>
								<th class="col-md-4">Možnosti</th>
							</tr>
						</thead>
						<tbody>
							<?php
								foreach ($edit->slova as $slovo) {
									?>
										<tr>
											<td><?=$slovo['slovo']?></td>
											<td><a href="edit/delslovo/<?=$slovo['id']?>" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span> Odstrániť</a></td>
										</tr>
									<?php
								}
							?>
						</tbody>
					</table>
				</div>
							
				<form class="form-horizontal" method="post">
					<div class="form-group">
						<label class="control-label col-sm-2" for="slova">Nové slová</label>	
						<div class="col-sm-8 nopadding">
							<input type="text" class="form-control" id="slova" name="slova">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-8 col-md-offset-2">
							<p>Ak chcete zadať viac slov súčastne, oddeľte ich čiarkou.</p>
						</div>
					</div>
					<div class="form-group"> 
						<label class="control-label col-sm-2"></label>		
						<div class="col-sm-8 nopadding">
							<button type="submit" class="btn btn-primary" name="pridaj_slovo">Pridať slová</button>
						</div>
					</div>
				</form>
				
				<h1>Vety</h1>
				<div class="padding-side-15">
					<table class="table table-striped table-style-1 table-hover table-bordered table-condensed">
						<thead>
							<tr>
								<th class="col-md-8">Veta</th>
								<th class="col-md-4">Možnosti</th>
							</tr>
						</thead>
						<tbody>
							<?php
								foreach ($edit->vety as $veta) {
									?>
										<tr>
											<td><?=$veta['veta']?></td>
											<td><a href="edit/delveta/<?=$veta['id']?>" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span> Odstrániť</a></td>
										</tr>
									<?php
								}
							?>
						</tbody>
					</table>
				</div>
							
				<form class="form-horizontal" method="post">
					<div class="form-group">
						<label class="control-label col-sm-2" for="veta">Nová veta</label>	
						<div class="col-sm-8 nopadding">
							<input type="text" class="form-control" id="veta" name="veta">
						</div>
					</div>
					<div class="form-group"> 
						<label class="control-label col-sm-2"></label>		
						<div class="col-sm-8 nopadding">
							<button type="submit" class="btn btn-primary" name="pridaj_vetu">Pridať vetu</button>
						</div>
					</div>
				</form>
			</div>
		<?php		
	} 
?>	

