-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Hostiteľ: 127.0.0.1
-- Čas generovania: St 12.Okt 2016, 16:50
-- Verzia serveru: 5.7.9
-- Verzia PHP: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáza: `spv`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `slova`
--

DROP TABLE IF EXISTS `slova`;
CREATE TABLE IF NOT EXISTS `slova` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slovo` varchar(30) COLLATE utf8_slovak_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

--
-- Sťahujem dáta pre tabuľku `slova`
--

INSERT INTO `slova` (`id`, `slovo`) VALUES
(16, ' najvyrukavičkovanejší'),
(15, ' mama'),
(13, 'ahoj');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `vety`
--

DROP TABLE IF EXISTS `vety`;
CREATE TABLE IF NOT EXISTS `vety` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `veta` text COLLATE utf8_slovak_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

--
-- Sťahujem dáta pre tabuľku `vety`
--

INSERT INTO `vety` (`id`, `veta`) VALUES
(3, 'Idem von.'),
(2, ',,Takto som to nechcel urobiť," ticho poznamenal Peter, ,,nebudeme pokračovať."');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
